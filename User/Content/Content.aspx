﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Content.aspx.cs" EnableEventValidation="false"
    ValidateRequest="false" Inherits="PowerEasy.Module.General.WebSite.User.Content.Content" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body>
    <pe:extendedsitemappath id="YourPosition" sitemapprovider="UserMapProvider" runat="server" />
    <form id="MainForm" runat="server">
    <asp:ScriptManager ID="SmContent" runat="server" ScriptMode="Release" EnablePartialRendering="true">
    </asp:ScriptManager>
    <div class="model_content_wrap">
        <div class="model_content_top">
            <h3>
                <pe:alternateliteral id="LblTitle" text="<%$ Res: User_Contents_Content_LblTitle,添加内容信息 %>"
                    alternatetext="<%$ Res: User_Contents_Content_LblTitle_AlternateText,修改内容信息 %>"
                    runat="Server" />
            </h3>
        </div>
        <div class="model_content_center">
            <ul class="add_model_content">
                <pe:extendedrepeater id="ContentForm" onitemdatabound="ContentForm_OnItemDataBound"
                    runat="server">
                    <ItemTemplate>
                        <li>
                            <label>
                                <%# Eval("FieldAlias")%>：<%# (Eval("Tips") == null || string.IsNullOrEmpty(Eval("Tips").ToString())) ? "" : "<br />" + Eval("Tips").ToString()%></label>
                            <pe:FieldControl ID="Field" runat="server" EnableNull='<%# (bool)Eval("EnableNull") %>'
                                FieldAlias='<%# Eval("FieldAlias")%>' Tips='<%# Eval("Tips") %>' FieldName='<%#Eval("FieldName")%>'
                                ControlType='<%# Eval("FieldType") %>' FieldLevel='<%# Eval("FieldLevel") %>' ModelId='<%# Eval("ModelId") %>'
                                IsAdminManage="false" Description='<%# Eval("Description")%>' Settings='<%# ((PowerEasy.Module.General.Model.CommonModel.FieldInfo)Container.DataItem).Settings %>'
                                Value='<%# Eval("DefaultValue") %>'>
                            </pe:FieldControl>
                        </li>
                    </ItemTemplate>
                </pe:extendedrepeater>
            </ul>
            <!--zyOffice begin-->
            <link type="text/css" rel="Stylesheet" href="/zyoffice/js/skygqbox.css" />
            <script type="text/javascript" src="/zyoffice/js/json2.min.js" charset="utf-8"></script>    
            <script type="text/javascript" src="/zyoffice/js/skygqbox.js" charset="utf-8"></script>
            <script type="text/javascript" src="/zyoffice/js/w.js" charset="utf-8"></script>
            <!--zyOffice end-->
            <script language="JavaScript" type="text/JavaScript">
                //zyOffice开始
                zyOffice.getInstance({});//加载控件
                                  
                //zyOffice结束
            </script>
            <div class="submit_model_content">
                <asp:Button ID="BtnSave" runat="server" CssClass="submit_button" Text="<%$ Res: User_Contents_Content_BtnSave,提交 %>"
                    OnClick="BtnSave_Click" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
